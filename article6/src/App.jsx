import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'

function App() {
  const [orderDate, setOrderDate] = useState('');
  const [deliveryDate, setDeliveryDate] = useState('');


  function handleSubmit(e) {
    e.preventDefault();
    const deliveryDate = calculateDeliveryDate(orderDate);
    setDeliveryDate(deliveryDate);
  }

  function calculateDeliveryDate(orderDate) {
    var holidays = ['2018-01-01', '2018-01-02', '2018-03-01', '2018-04-06', '2018-04-13', '2018-04-14', '2018-04-15', '2018-04-16', '2018-05-01'];
    var productionTime = 1;
    var shippingTime = 1;
    
    // Parse the order date
    var orderDateObj = new Date(orderDate);
    
    // Check if the order date is a Sunday
    if (orderDateObj.getDay() === 0) {
      orderDateObj.setDate(orderDateObj.getDate() + 1); // Add 1 day to make it Monday
    }
    
    // Add production time
    orderDateObj.setDate(orderDateObj.getDate() + productionTime);
    
    // Add shipping time
    var deliveryDate = new Date(orderDateObj);
    deliveryDate.setDate(deliveryDate.getDate() + shippingTime);
    
    console.log(('0' + (deliveryDate.getMonth() + 1)))
    // Check if the delivery date is a holiday or a Sunday
    '2018-04'
    while (holidays.indexOf(deliveryDate.getFullYear() + '-' + ('0' + (deliveryDate.getMonth() + 1)).slice(-2) + '-' + ('0' + deliveryDate.getDate()).slice(-2)) != -1 || deliveryDate.getDay() === 0) {
      deliveryDate.setDate(deliveryDate.getDate() + 1);
    }
    
    // Return the delivery date in the format YYYY-MM-DD
    var year = deliveryDate.getFullYear();
    var month = ('0' + (deliveryDate.getMonth() + 1)).slice(-2);
    var day = ('0' + deliveryDate.getDate()).slice(-2);
    return year + '-' + month + '-' + day;
  }
  
  

  // console.log(calculateDeliveryDate('2018-04-13')); // Output: 2018-04-18

  return (
    <div className="App">
      <form onSubmit={handleSubmit}>
        <label>
          Enter the order date:
          <input type="text" value={orderDate} onChange={(e) => setOrderDate(e.target.value)} />
        </label>
        <button type="submit">Calculate delivery date</button>
      </form>
      {deliveryDate && <div>The estimated delivery date is {deliveryDate}</div>}
    </div>
  )
}

export default App
